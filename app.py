from flask import Flask
import math
app = Flask(__name__)


@app.route('/integrate/<low>/<up>')
def integrate(low,up):
    result= ""
    for k in range(7):
        N = 10**k
        interval = abs(float(up)-float(low))/N
        somme=0
        for j in range(N):
            x=(j+0.5)*interval
            somme+= interval*abs(math.sin(x))
        result = result + "{}".format(somme) + " , "
    return result

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9999)

