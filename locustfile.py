#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 11 15:06:50 2022

@author: yossitapiero
"""

import time
from locust import HttpUser, task, between

class QuickstartUser(HttpUser):  
    wait_time = between(1, 6)
    @task
    def hello_world(self):
        self.client.get("/")
